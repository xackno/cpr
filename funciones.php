<?php

	require __DIR__ . '/autoload.php'; 
	use Mike42\Escpos\Printer;
	use Mike42\Escpos\EscposImage;
	use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
	// use Mike42\Escpos\PrintConnectors\FilePrintConnector;


//############################FUNCIONES DE IMPRESIÓN######################################
function venta($json){
	echo "Ventas al contado";
		$config=file_get_contents("config/config.json");
		$dataConfig=json_decode($config,JSON_FORCE_OBJECT);

	  //smb://Mostrador1-pc/epson
	$connector = new WindowsPrintConnector($dataConfig["impresora"]);
	// $connector = new FilePrintConnector("php://output");
	$printer = new Printer($connector);
	
		try{
		$logo = EscposImage::load("img/logo2.png", false);
	    $printer->bitImage($logo);
		}catch(Exception $e)
		{}

	$printer->setPrintLeftMargin (0);
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->setLineSpacing (1);//espacio de interlineado valor 1-255
	$printer->setPrintLeftMargin (0);
	$printer->setPrintWidth (1000);
	$printer->text("Domicilio conocido \nSan Miguel El Grande. \n");
	$printer->setReverseColors (true);
	$numero = $json["venta"];
	$numeroConCeros = str_pad($numero, 8, "0", STR_PAD_LEFT);
	$printer->text("............VENTA..#".$numeroConCeros."...\n");
	$printer->setReverseColors (false);	
	$printer->setEmphasis(false);
	$printer->text("Atendido por:".$json["cajero"]."\n");
	$printer->text("FECHA:".$json["fecha"]."\n");
	// $printer->text("#----UNI---DESC---------$----$$\n");
	$printer->setReverseColors (true);
	$printer->text("DESC------CANT---UNI--PREC---$$\n");
	$printer->setReverseColors (false);
	$printer->setJustification(Printer::JUSTIFY_LEFT);
		for($x=0;$x<count($json["id_art"]);$x++){
			// $printer->text("  ".$json["cantidad"][$x]."  ".$json["unidad"][$x]." ".$json["descripcion"][$x]." ".$json["precio"][$x]." ".$json["importe"][$x]."\n");
			$printer->text($json["descripcion"][$x]."\n".$json["cantidad"][$x]." ".$json["unidad"][$x]."   X   $".$json["precio"][$x]."  =  $".$json["importe"][$x]."\n");
			$printer->text("--------------------------------\n");
		}
	$printer->setJustification(Printer::JUSTIFY_RIGHT);
	$printer->text("Total:$".$json["total"]."\n");
	$printer->text("Efectivo:$".$json["efectivo"]."\n");
	$printer->setLineSpacing (100);
	$printer->text("Cambio:$".$json["cambio"]."\n");
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->setBarcodeWidth (6);
	$printer->setBarcodeHeight(50);
	$printer->barcode(intval($numeroConCeros));//(string $content,int $type(65-73)) //or 72
	$printer->text($numeroConCeros."\n");
	$printer->selectPrintMode(Printer::MODE_FONT_B);
	$printer->text("***GRACIAS POR SU PREFERENCIA***\n");
	$printer->feed();
	$printer->cut();
	$printer->close();
}

function reimpresion($json){
		$config=file_get_contents("config/config.json");
		$dataConfig=json_decode($config,JSON_FORCE_OBJECT);

	  //smb://Mostrador1-pc/epson
	$connector = new WindowsPrintConnector($dataConfig["impresora"]);
	// $connector = new FilePrintConnector("php://output");
	$printer = new Printer($connector);
	
		try{
		$logo = EscposImage::load("img/logo2.png", false);
	    $printer->bitImage($logo);
		}catch(Exception $e)
		{}

	$printer->setPrintLeftMargin (0);
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->setLineSpacing (1);//espacio de interlineado valor 1-255
	$printer->setPrintLeftMargin (0);
	$printer->setPrintWidth (1000);
	$printer->text("Domicilio conocido \nSan Miguel El Grande. \n");
	$printer->setReverseColors (true);
	$numero = $json["venta"];
	$numeroConCeros = str_pad($numero, 8, "0", STR_PAD_LEFT);
	$printer->text("Reimpresión venta #".$numeroConCeros."...\n");
	$printer->setReverseColors (false);	
	$printer->setEmphasis(false);
	$printer->text("Atendido por:".$json["cajero"]."\n");
	$date=$json["fecha"];
	$date=explode("T", $date);
	$fecha=$date[0];
	$hora=explode(".",$date[1]);
	$hora=$hora[0];
	$printer->text("FECHA:".$fecha."HORA:".$hora."\n");
	// $printer->text("#----UNI---DESC---------$----$$\n");
	$printer->setReverseColors (true);
	$printer->text("DESC------CANT---UNI--PREC---$$\n");
	$printer->setReverseColors (false);
	$printer->setJustification(Printer::JUSTIFY_LEFT);
		for($x=0;$x<count($json["id_art"]);$x++){
			// $printer->text("  ".$json["cantidad"][$x]."  ".$json["unidad"][$x]." ".$json["descripcion"][$x]." ".$json["precio"][$x]." ".$json["importe"][$x]."\n");
			$printer->text($json["descripcion"][$x]."\n".$json["cantidad"][$x]." ".$json["unidad"][$x]."   X   $".$json["precio"][$x]."  =  $".$json["cantidad"][$x]*$json["precio"][$x]."\n");
			$printer->text("--------------------------------\n");
		}
	$printer->setJustification(Printer::JUSTIFY_RIGHT);
	$printer->text("Total:$".$json["total"]."\n");
	$printer->text("Efectivo:$".$json["efectivo"]."\n");
	$printer->setLineSpacing (100);
	$printer->text("Cambio:$".number_format($json["efectivo"]-$json["total"],2)."\n");
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->setBarcodeWidth (6);
	$printer->setBarcodeHeight(50);
	$printer->barcode(intval($numeroConCeros));//(string $content,int $type(65-73)) //or 72
	$printer->text($numeroConCeros."\n");
	$printer->selectPrintMode(Printer::MODE_FONT_B);
	$printer->text("***GRACIAS POR SU PREFERENCIA***\n");
	$printer->feed();
	$printer->cut();
	$printer->close();
}





function credito(){
	echo "ventas a creditos";
}
function pedido($json){
	echo "Ventas al contado";
		$config=file_get_contents("config/config.json");
		$dataConfig=json_decode($config,JSON_FORCE_OBJECT);

	  //smb://Mostrador1-pc/epson
	$connector = new WindowsPrintConnector($dataConfig["impresora"]);
	// $connector = new FilePrintConnector("php://output");
	$printer = new Printer($connector);


	//##############################################################
	//########LOGO########################
		$printer->setPrintLeftMargin (0);
			$printer->setJustification(Printer::JUSTIFY_CENTER);
			try{
			$logo = EscposImage::load("img/logo2.png", false);
		    $printer->bitImage($logo);
			}catch(Exception $e){}
		$printer->setPrintLeftMargin (0);
		$printer->setJustification(Printer::JUSTIFY_CENTER);
		$printer->setLineSpacing (1);//espacio de interlineado valor 1-255
		$printer->setPrintLeftMargin (0);
		$printer->setPrintWidth (1000);
		$printer->text("Carnes del campo \n Col. Centro, Tlaxiaco, Oax.\n");
		$printer->setReverseColors (true);
		$numero = $json["pedido"]["id"];
		$numeroConCeros = str_pad($numero, 8, "0", STR_PAD_LEFT);
		$printer->text("............PEDIDO..#".$numeroConCeros."...\n");
		$printer->setReverseColors (false);	
		$printer->setEmphasis(false);
		$printer->text("Atendido por:".$json["cajero"]."\n");
		$printer->text("FECHA:".$json["fecha"]."\n");
		$printer->text("Enviar a:".$json["nombre_cliente"]."\n");
		$printer->text("Domicilio:".$json["colonia"].$json["localidad"]."\n");
		$printer->text("REFERENCIA:");
		$printer->text('"'.$json["descripcion_lugar_envio"].'"'."\n");
		// $printer->text("#----UNI---DESC---------$----$$\n");
		$printer->setReverseColors (true);
		$printer->text("DESC------CANT---UNI--PREC---$$\n");
		$printer->setReverseColors (false);
		$printer->setJustification(Printer::JUSTIFY_LEFT);
			for($x=0;$x<count($json["desc_pro"]);$x++){
				// $printer->text("  ".$json["cantidad"][$x]."  ".$json["unidad"][$x]." ".$json["descripcion"][$x]." ".$json["precio"][$x]." ".$json["importe"][$x]."\n");
				$printer->text($json["desc_pro"][$x]."\n".$json["cantidad"][$x]." ".$json["unidad"][$x]."   X   $".$json["precio"][$x]."  =  $".$json["subtotalProducto"][$x]."\n");
				$printer->text("--------------------------------\n");
			}
		$printer->setJustification(Printer::JUSTIFY_RIGHT);
		$printer->text("Importe:$".$json["importe"]."\n");
		$printer->text("Costo envio:$".$json["costo_envio"]."\n");
		$printer->text("Subtotal:$".$json["subtotal"]."\n");
		$printer->text("Descuento:$".$json["descuento"]."\n");
		$printer->setLineSpacing (80);
		$printer->text("TOTAL:$".$json["total"]."\n");
		$printer->setJustification(Printer::JUSTIFY_CENTER);
		$printer->setLineSpacing (100);
		$printer->setBarcodeWidth (6);
		$printer->setBarcodeHeight(50);
		$printer->barcode(intval($numeroConCeros));//(string $content,int $type(65-73)) //or 72
		$printer->text($numeroConCeros."\n");
		$printer->selectPrintMode(Printer::MODE_FONT_B);
		$printer->text("***GRACIAS POR SU PREFERENCIA***\n");

	
	//########LOGO########################
	$printer->setPrintLeftMargin (0);
		$printer->setJustification(Printer::JUSTIFY_CENTER);
		try{
		$logo = EscposImage::load("img/logo2.png", false);
	    $printer->bitImage($logo);
		}catch(Exception $e){}
	$printer->setPrintLeftMargin (0);
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->setLineSpacing (1);//espacio de interlineado valor 1-255
	$printer->setPrintLeftMargin (0);
	$printer->setPrintWidth (1000);
	$printer->text("Carnes del campo \n Col. Centro, Tlaxiaco, Oax.\n");
	$printer->setReverseColors (true);
	$numero = $json["pedido"]["id"];
	$numeroConCeros = str_pad($numero, 8, "0", STR_PAD_LEFT);
	$printer->text("............PEDIDO..#".$numeroConCeros."...\n");
	$printer->setReverseColors (false);	
	$printer->setEmphasis(false);
	$printer->text("Atendido por:".$json["cajero"]."\n");
	$printer->text("FECHA:".$json["fecha"]."\n");
	$printer->text("Enviar a:".$json["nombre_cliente"]."\n");
	$printer->text("Domicilio:".$json["colonia"].$json["localidad"]."\n");
	$printer->text("REFERENCIA:");
	$printer->text('"'.$json["descripcion_lugar_envio"].'"'."\n");
	// $printer->text("#----UNI---DESC---------$----$$\n");
	$printer->setReverseColors (true);
	$printer->text("DESC------CANT---UNI--PREC---$$\n");
	$printer->setReverseColors (false);
	$printer->setJustification(Printer::JUSTIFY_LEFT);
		for($x=0;$x<count($json["desc_pro"]);$x++){
			// $printer->text("  ".$json["cantidad"][$x]."  ".$json["unidad"][$x]." ".$json["descripcion"][$x]." ".$json["precio"][$x]." ".$json["importe"][$x]."\n");
			$printer->text($json["desc_pro"][$x]."\n".$json["cantidad"][$x]." ".$json["unidad"][$x]."   X   $".$json["precio"][$x]."  =  $".$json["subtotalProducto"][$x]."\n");
			$printer->text("--------------------------------\n");
		}
	$printer->setJustification(Printer::JUSTIFY_RIGHT);
	$printer->text("Importe:$".$json["importe"]."\n");
	$printer->text("Costo envio:$".$json["costo_envio"]."\n");
	$printer->text("Subtotal:$".$json["subtotal"]."\n");
	$printer->text("Descuento:$".$json["descuento"]."\n");
	$printer->setLineSpacing (80);
	$printer->text("TOTAL:$".$json["total"]."\n");
	$printer->setJustification(Printer::JUSTIFY_CENTER);
	$printer->text("FIRMA DE RECIBIDO\n\n ______________________\n Para devolución: especifique al dorso del TICKET el motivo.\n");
	$printer->setLineSpacing (100);
	$printer->setBarcodeWidth (6);
	$printer->setBarcodeHeight(50);
	$printer->barcode(intval($numeroConCeros));//(string $content,int $type(65-73)) //or 72
	$printer->text($numeroConCeros."\n");
	$printer->selectPrintMode(Printer::MODE_FONT_B);
	$printer->text("***GRACIAS POR SU PREFERENCIA***\n");
	$printer->feed();
	$printer->cut();
	$printer->close();
}

	




?>