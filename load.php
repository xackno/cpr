<!DOCTYPE html>
<html>
<head>
	<title>printer</title>
	<meta charset="utf-8">
	<meta lang="es">
</head>

<?php
 require("funciones.php");


session_start();
$status="";
$_SESSION["reconfig"]="no";
$config=file_get_contents("config/config.json");
$dataConfig=json_decode($config,JSON_FORCE_OBJECT);

	$arrContextOptions=array(
    "ssl"=>array(
        "verify_peer"=>false,
        "verify_peer_name"=>false,
    	),
	);  
	$file=file_get_contents($dataConfig["url_completo"], false, stream_context_create($arrContextOptions));
	$json=json_decode($file,JSON_FORCE_OBJECT);
	 
	  // pedido($json);

	// $_SESSION["proceso"]=2;
	if (isset($_SESSION["proceso"])) {//si existe la variable
		if ($_SESSION["proceso"]==$json["proceso"]) {//si es igual significa que ya se imprimio
			$status="sin proceso de impresión";
			header("Refresh:2; ");
		}else{//si no es igual IMPRIMIR EL TICKET e igualar de nuevo el proceso
				header("Refresh:5; ");
				$status="imprimiento...";
			if($json["tipo"]=="venta"){
				 venta($json);
				
			}else if($json["tipo"]=="credito"){
				 credito();
			}else if($json["tipo"]=="pedido"){
				pedido($json);
			}else if($json["tipo"]=="reimpresion"){
				reimpresion($json);
			}

			$_SESSION["proceso"]=$json["proceso"];
		}//fin else de imprimir
	}else{
		$_SESSION["proceso"]=0;
		// echo "No definido";
		$status="Sin datos";
		header("Refresh:2;");
	}//fin else de no existe la variable session 
?>

<body style="background: linear-gradient(to right, rgba(0,168,121,1) 0%, rgba(0,138,99,0.92) 44%, rgba(0,0,0,0.87) 74%, rgba(0,0,0,0.82) 100%);">

	<button onclick="reconf();" style="background: #273746;padding:6px;color:white;border-radius: 5px;border:0px">Configuración</button>
	<!-- ###########################card############################################ -->
		<div style="margin:auto;width: 20%;height: 700px;background: #FFFFF0">
			<div style="margin:0px;height: 50px;text-align: center" >
				<?php
				print_r($status);
				?><hr>
			</div>
			<div style="text-align: center" id="div-header">
				<img src="img/logo2.png" style="width: 150px">
				<p><b>Carnes del campo</b></p>
				<p>Col. Centro, Tlaxiaco, Oax.</p>
			</div>
			<!-- ##############################div body############################# -->
			<div class="div-body">

				<?php
					if ($json["tipo"]=="venta") {
						?>
						<p style="font-size: 20px;text-align: center;width: 100%;background: black;color:white">Venta  #<?php echo $json["venta"] ?></p>
						<p>Atendido por: <?php echo "<b>".$json["cajero"]."</b>"; ?></p>
						<p>Fecha: <?php echo $json["fecha"]?></p>
						<table style="width: 100%">
							<thead >
								<tr><th>#</th><th>uni</th><th>desc</th><th>$</th><th>$$</th></tr>
							</thead>
							<tbody>
								<?php
									for($x=0;$x<count($json["id_art"]);$x++){
										?>
										<tr>
											<td><?php echo $json["cantidad"][$x]; ?></td>
											<td><?php echo $json["unidad"][$x]; ?></td>
											<td><?php echo $json["descripcion"][$x]; ?></td>
											<td><?php echo $json["precio"][$x]; ?></td>
											<td><?php echo $json["importe"][$x]; ?></td>
										</tr>
										<?php
									}

								?>

							</tbody>
						</table>
						<hr>
						<div style="width: 50%;float:right;margin-right: 0px;" class="div-footer">
							<p><span>Total:$</span><span><b><?php echo $json["total"]?></b></span></p>
							<p><span>Pago:$</span><span><b><?php echo $json["efectivo"]?></b></span></p>
							<p><span>Cambio:$</span><span><b><?php echo $json["cambio"]?></b></span></p>
						</div><br><br><br>
						<hr>
						<div style="text-align: center">
							<p>***gracias por su preferencia***</p>
						</div>
						
						
						
						<?php
					}//fin if venta
					if ($json["tipo"]=="reimpresion") {
						?>
						<p style="font-size: 20px;text-align: center;width: 100%;background: black;color:white">reimpresión Venta #<?php echo  str_pad($json["venta"], 8, "0", STR_PAD_LEFT) ?></p>
						<p>Atendido por: <?php echo "<b>".$json["cajero"]."</b>"; ?></p>
						<p><?php 
						$date=$json["fecha"];
						$date=explode("T", $date);
						$fecha=$date[0];
						$hora=explode(".",$date[1]);
						$hora=$hora[0];

						echo "FECHA:".$fecha."  HORA: ".$hora ?></p>
						<table style="width: 100%">
							<thead >
								<tr><th>#</th><th>uni</th><th>desc</th><th>$</th><th>$$</th></tr>
							</thead>
							<tbody>
								<?php
									for($x=0;$x<count($json["id_art"]);$x++){
										?>
										<tr>
											<td><?php echo $json["cantidad"][$x]; ?></td>
											<td><?php echo $json["unidad"][$x]; ?></td>
											<td><?php echo $json["descripcion"][$x]; ?></td>
											<td><?php echo $json["precio"][$x]; ?></td>
											<td><?php echo $json["cantidad"][$x]*$json["precio"][$x] ?></td>
										</tr>
										<?php
									}

								?>

							</tbody>
						</table>
						<hr>
						<div style="width: 50%;float:right;margin-right: 0px;" class="div-footer">
							<p><span>Total:$</span><span><b><?php echo $json["total"]?></b></span></p>
							<p><span>Pago:$</span><span><b><?php echo $json["efectivo"]?></b></span></p>
							<p><span>Cambio:$</span><span><b><?php echo number_format($json["efectivo"]-$json["total"],2)?></b></span></p>
						</div><br><br><br>
						<hr>
						<div style="text-align: center">
							<p>***gracias por su preferencia***</p>
						</div>
						
						
						
						<?php
					}//fin if venta
					else if($json["tipo"]=="pedido"){
						?>
						<p style="font-size: 20px;text-align: center;width: 100%;background: black;color:white">Pedido  #<?php echo $json["pedido"]["id"] ?></p>
						<p>Cajero:<?php echo $json["cajero"];?></p>
						<p>Fecha: <?php echo $json["fecha"];?></p>
						<p>Envia a:<?php echo $json["nombre_cliente"]?></p>
						<p>Domicilio:<?php echo $json["colonia"].$json["localidad"];?></p>
						<p>Referencia: <?php echo '"'.$json["descripcion_lugar_envio"].'"';?></p>
						<table style="width: 100%">
							<thead >
								<tr><th>#</th><th>uni</th><th>desc</th><th>$</th><th>$$</th></tr>
							</thead>
							<tbody>
								<?php
									for($x=0;$x<count($json["desc_pro"]);$x++){
										?>
										<tr>
											<td><?php echo $json["cantidad"][$x]; ?></td>
											<td><?php echo $json["unidad"][$x]; ?></td>
											<td><?php echo $json["desc_pro"][$x]; ?></td>
											<td><?php echo $json["precio"][$x]; ?></td>
											<td><?php echo $json["subtotalProducto"][$x]; ?></td>
										</tr>
										<?php
									}

								?>

							</tbody>
						</table>
						<hr>
						<div style="width: 50%;float:right;margin-right: 0px;" class="div-footer">
							<p><span>Importe:$</span><span><b><?php echo $json["importe"]?></b></span></p>
							<p><span>Costo de envio:$</span><span><b><?php echo $json["costo_envio"]?></b></span></p>
							<p><span>Subtotal:$</span><span><b><?php echo $json["subtotal"]?></b></span></p>
							<p><span>Descuento:$</span><span><b><?php echo $json["descuento"]?></b></span></p>
							<p><span>TOTAL A PAGAR:$</span><span><b><?php echo $json["total"]?></b></span></p>
						</div><br><br><br>
						<br><br>
						<div style="text-align: center">
							<hr>
							<p>FIRMA DE RECIBIDO</p>
							<br>
							___________________
							<p>Para devolución:especifique al dorso del Ticket el motivo</p>
							<p>***gracias por su preferencia***</p>
						</div>
						<?php

					}

				?>
				
			</div>

		</div>


</body>
</html>



<style type="text/css">
	table thead tr th{border: 1px solid black}
	#div-header p,.div-body p{
		margin: 0px;padding: 0px;border:0px;
	}
	.div-footer b{float: right;}
</style>
<script type="text/javascript">
	function reconf(){
		window.location='index.php'
		<?php $_SESSION["reconfig"]="si";?>
	}
</script>
